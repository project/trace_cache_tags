<?php

namespace Drupal\trace_cache_tags;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\fastly\EventSubscriber\SurrogateKeyGenerator;
use GuzzleHttp\ClientInterface;

/**
 * Cache tags invalidator implementation.
 */
class CacheTagsInvalidator implements CacheTagsInvalidatorInterface {

  /**
   * Drupal\Core\Logger\LoggerChannelFactory definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Constructs a new CacheTagsInvalidator object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Drupal logger.
   */
  public function __construct(LoggerChannelFactory $logger) {
    $this->logger = $logger->get('trace_cache_tags');
  }

  /**
   * {@inheritdoc}
   */
  public function invalidateTags(array $tags) {
    $tag_list = '';
    foreach ($tags as $tag) {
      $message = t("Invalidate @tag", ['@tag' => $tag]);
      $this->logger->notice($message);
    }
  }

}
