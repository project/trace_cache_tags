DESCRIPTION
-----------
Logs all cache tag invalidation to dblog.

CONFIGURATION
-------------
No configuration required, just install the module.
